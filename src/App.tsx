import * as React from 'react';
import './App.css';
import Header from './layout/Header';
import Footer from './layout/Footer';
import CardDaily from './components/CardDaily';
import CardPast from './components/CardPast';
import InnerPast from './components/InnerPast';
import InnerDaily from './components/InnerDaily';
import Empty from './layout/Empty';
import {cities, weatherInterface} from './data/helpers';



const App: React.FunctionComponent = () => {
	const [errorDailyWeather, setErrorDailyWeather] = React.useState('')  
	const [errorPastWeather, setErrorPastWeather] = React.useState('')
	const [dailyWeatherData, setDailyWeatherData] = React.useState<weatherInterface>()
	const [pastWeatherData, setPastWeatherData] = React.useState<weatherInterface>()


	const requestToDailyWeather = (requestUrl: string) => {
		return fetch(requestUrl)
		.then(response => {
			if (!response.ok) {        
				setErrorDailyWeather(response.statusText)
			}
			return response.json() 
		})  
		.then(result => { 
			if (result.daily) {
				setErrorDailyWeather('')	
				const shallowCopyResultDaily = [...result.daily]
				const getValueFromParams = Object.values(shallowCopyResultDaily)

				const getDateFromValues:number[] = getValueFromParams.map(item => item.dt);
				const getTempFromValues:number[] = getValueFromParams.map(item => Math.round(item.temp.day));
				const getIconFromValues:string[] = getValueFromParams.map(item => item.weather[0].icon);

				setDailyWeatherData({date: getDateFromValues, temp: getTempFromValues, icon: getIconFromValues})
		
			}
		})
	}

	const requestToPastWeather = (requestUrl: string) => {
		return fetch(requestUrl)
		.then(response => {
			if (!response.ok) { 
				setErrorPastWeather(response.statusText)
			}
			return response.json() 
		})  
		.then(result => { 
			if (result.current) {
				setErrorPastWeather('')
				setPastWeatherData({
					date: result.current.dt, 
					temp: Math.round(result.current.temp), 
					icon: result.current.weather[0].icon
				})
			}
		})
	}


	const showDailyCard = () => {
		if (errorDailyWeather || !dailyWeatherData) {
			return  <Empty/> 
		}
		return  <InnerDaily weatherDataProps={dailyWeatherData} />
	}

	const showPastCard = () => {
		if (errorPastWeather || !pastWeatherData) {
			return  <Empty/> 
		}    
		return  <InnerPast weatherDataProps={pastWeatherData} />
	}



	return (

	<div className="page">

		<Header />

			<section className="content">   

				<div className="forecast">

					<CardDaily 
						title="7 Days Forecast"
						cityArray={cities} 
						apiRequest={requestToDailyWeather} 
						></CardDaily>

					{showDailyCard()}

				</div>

				<div className="forecast forecast-past">

					<CardPast 
						title="Forecast for a Date in the Past" 
						cityArray={cities}  
						apiRequest={requestToPastWeather}
						></CardPast>

					{showPastCard()}

					</div>

				</section>

			<Footer />

		</div>

	)

}  

export default App