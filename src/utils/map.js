/**
 * @template T, V
 * @param {(element: T) => V} callback
 * @param {T[]} collection
 * @returns {V[]}
 */

const realiseMap = function(callback, collection) {
	let mappedArray = collection.reduce(function(accumulator, currentValue) {
		return [...accumulator, callback(currentValue)]
	}, [])
	return mappedArray
}
