import * as React from 'react';
import {API_KEY, BASE_URL} from '../data/config';
import {City} from '../data/helpers';


interface CardProps {
	title: string,
	cityArray: City[],
	apiRequest: (url: string) => void
}

const CardDaily: React.FC<CardProps> = (props: CardProps) => {

	const urlDaily = 'data/2.5/onecall?'
	const [cityText, setCityText] =  React.useState('Select city')

	const getCityElements:City[]  = [...props.cityArray]

	const [visibleCityList, setVisibleCityList] = React.useState(false);
	const toggleCities = () => setVisibleCityList(!visibleCityList);

	const [openDropdown, setOpenDropdown] =  React.useState(false);
	const toggleClass = () => {
		setOpenDropdown(!openDropdown);
	};

	const makeRequest = (lat: number, lon: number) => {		
		props.apiRequest(`${BASE_URL}${urlDaily}lat=${lat}&lon=${lon}&exclude=current,minutely,hourly,alerts&appid=${API_KEY}&units=metric`)	
	}


	return(

		<React.Fragment>

			<h3 className="forecast__title">
				{props.title}
			</h3>
			
			<div className={openDropdown ? 'forecast__select forecast__select-up': 'forecast__select'} >
			
				<span onClick={								
						(event: React.MouseEvent<HTMLElement>) => {
							toggleCities()
							toggleClass() 									
						}						
					}>
					{cityText}
				</span>

				{visibleCityList &&
					<ul className="forecast__cities">	

						{getCityElements.map((cityElement: {name: string, lat: number, lon: number}) => {
								return (

									<li key={cityElement.name} 
										onClick={				
											(event: React.MouseEvent<HTMLElement>) => {
												setCityText(cityElement.name)	
												setVisibleCityList(!visibleCityList)
												toggleClass() 
												makeRequest(cityElement.lat, cityElement.lon)
											}								
										}>{cityElement.name}
									</li>
								)						

							})			
						}

					</ul>
				}

			</div>			

		</ React.Fragment>

	)

}

export default CardDaily