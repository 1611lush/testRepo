import * as React from 'react';
import {weatherInterface} from '../data/helpers';

interface InnerDailyProps {
	weatherDataProps?: weatherInterface	
}



const InnerDaily: React.FC<InnerDailyProps> = (props: InnerDailyProps) => {	
	const monthNames: string[] = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
	const getValueFromProps = Object.values(props.weatherDataProps!)

	const showWeatherOnWeekday = getValueFromProps[0].map((weatherOnWeekday: number, index: number) => {
		const getDateFromProps = new Date(weatherOnWeekday*1000).toLocaleString("en-US").split(",")[0]	
		const getMonthName: string = getDateFromProps.split("/")[0] 		
		const getFinalDate: string = (getDateFromProps.split("/")[1] + ' ' + monthNames[+getMonthName - 1] + ' ' + getDateFromProps.split("/")[2])

		return (
			<div className="forecast__result forecast__result_full" key={index}>
				<h4 className="forecast__date">{getFinalDate}</h4>
				<img src={`http://openweathermap.org/img/wn/${getValueFromProps[2][index]}@2x.png`} alt="" className="forecast__img " />
				<p className="forecast__temp">+{getValueFromProps[1][index]}°</p>
			</div>
		)
	})
		

		
	const [margin, setMargin] = React.useState(0);

	return(	
		
				<div className="slider">

					{margin !== 0 && 
						<button className="slider__btn slider__left" 
							onClick={				
								(event: React.MouseEvent<HTMLElement>) => {
									setMargin(margin + 184)	
								}
							}>
						</button>
					}		
					<div className="slider__inner" style = {{marginLeft : margin}}>

						{
							showWeatherOnWeekday
						}	

					</div>

					{ margin > -915 &&
						<button className="slider__btn slider__right" 
							onClick={				
								(event: React.MouseEvent<HTMLElement>) => {
									setMargin(margin - 184)
								}
							}>
						</button>
					}	
				</div>

		)

	}
	

export default InnerDaily