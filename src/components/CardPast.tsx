import * as React from 'react';
import {API_KEY, BASE_URL} from '../data/config';
import {City} from '../data/helpers';

interface CardProps {
	title: string,	
	cityArray: City[],
	apiRequest: (url:string) => void
}


const CardPast: React.FC<CardProps> = (props: CardProps) => {

	const urlPast = 'data/2.5/onecall/timemachine?'
	const [cityText, setCityText] =  React.useState('Select city')

	const getCityElements:City[]  = [...props.cityArray]

	const [visibleCityList, setVisibleCityList] = React.useState(false);
	const toggleCities = () => setVisibleCityList(!visibleCityList);


	const [openDropdown, setOpenDropdown] =  React.useState(false);
	const toggleOpenDropdownClass = () => {
		setOpenDropdown(!openDropdown);
	};	

	const [choosenDay, setChoosenDay] = React.useState<number>()

	const [latInput, setLatInput] = React.useState<number>()
	const [lonInput, setLonInput] = React.useState<number>()

	
	const makeRequestWithDate = (target: string) => {
		const formattedDate = parseInt((new Date(target).getTime() / 1000).toFixed(0))
		setChoosenDay(formattedDate)
		props.apiRequest(`${BASE_URL}${urlPast}lat=${latInput}&lon=${lonInput}&dt=${formattedDate}&appid=${API_KEY}&units=metric`)	
	}
	const makeRequest = (lat: number, lon: number) => {		
		choosenDay && props.apiRequest(`${BASE_URL}${urlPast}lat=${lat}&lon=${lon}&dt=${choosenDay}&appid=${API_KEY}&units=metric`)	
	}

	const maxDate = new Date().toISOString().split("T")[0]
	const fivePastDays = Date.now() - 432000000
	const minDate = new Date(fivePastDays).toISOString().split("T")[0]


	return(

		<React.Fragment>
			<h3 className="forecast__title">
				{props.title}
			</h3>

			<div className="forecast__inputs">
				<div className={openDropdown ? 'forecast__select forecast__select-up': 'forecast__select'} >
		
					<span onClick={				
							(event: React.MouseEvent<HTMLElement>) => {
								toggleCities()
								toggleOpenDropdownClass() 
							}			
						}>
						{cityText}
					</span>

					{visibleCityList &&
						<ul className="forecast__cities">						
							
							{getCityElements.map((cityElement: {name: string, lat: number, lon: number}) => {

								return (
									<li key={cityElement.name} 
										onClick={							
											(event: React.MouseEvent<HTMLElement>) => {
												setCityText(cityElement.name)	
												setVisibleCityList(!visibleCityList)
												toggleOpenDropdownClass() 
												setLatInput(cityElement.lat)
												setLonInput(cityElement.lon)
												makeRequest(cityElement.lat, cityElement.lon)
											}				
										}>{cityElement.name} </li>
									)						

								})			
							}

						</ul>
					}

				</div>		
				
				<div className="forecast__calendar" >
					<input 
						type="date" 
						placeholder="Select date"  
						name="datePast" 
						className="forecast__input" 
						min={minDate} 
						max={maxDate}
						onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
							makeRequestWithDate(event.target.value)
						}}
						defaultValue={''}
					/>	
					<span className="forecast__calendar-icon"></span>
				</div>

			</div>	

		</ React.Fragment>
	)

}

export default CardPast