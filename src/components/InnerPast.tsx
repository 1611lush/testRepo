import * as React from 'react';
import {weatherInterface} from '../data/helpers';

interface InnerPastProps {
	weatherDataProps?: weatherInterface
}



const InnerPast: React.FC<InnerPastProps> = (props: InnerPastProps) => {
	const getValueFromProps = Object.values(props.weatherDataProps!)
	const monthNames: string[] = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
	const getDateFromProps = new Date(getValueFromProps[0]*1000).toLocaleString("en-US").split(",")[0]	
	const getMonthName: string = getDateFromProps.split("/")[0]
	const getFinalDate: string = (getDateFromProps.split("/")[1] + ' ' + monthNames[+getMonthName - 1] + ' ' + getDateFromProps.split("/")[2])


	return(	

			<div className="forecast__result forecast__result_full">						
				<p className="forecast__date">{getFinalDate}</p>	
				<img src={`http://openweathermap.org/img/wn/${getValueFromProps[2]}@2x.png`}  alt="" className="forecast__img" />
				<p className="forecast__temp">+{getValueFromProps[1]}°</p>
			</div>	

	)
}
	

export default InnerPast